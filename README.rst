Tools and Tooling for CI/CD
==============================

This repo holds tools and tooling for
Continuous Integration and Continuous Delivery
in `Tryton Community`.

This project provides:

* **CI/CD pipeline configurations**, which can be included in
  your project by just adding three lines.

  This includes support for uploading the built package to PyPI
  and to the project's package registry at gitlab/heptapod.
  See ``doc/ci-templates.rst`` for details.
