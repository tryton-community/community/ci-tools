****************************************
CI/CD Pipeline Configurations
****************************************

The directory `_ci-templates` provides CI/CD pipeline configurations
for gitlab/heptapod.

To use them, simply put this into the ``.gitlab-ci.yml`` file of your
project, replacing ``68`` according to the Tryton version of your module::

  include:
    - project: tryton-community/community/ci-tools
      file: _ci-templates/tryton68-module.yml

Nothing else is required.
Anyhow by setting some `variables`_ you can adjust the behavior.

Features
=========

This pipeline configurations includes support for

* Uploading the built package to PyPI
  and to the project's package registry at gitlab/heptapod.
  See `Controlling Deployment`_ for details.

* Testing the documentation, including the Readme file and the Changelog.

* Running the test suite for all Python versions
  supported by the respective Tryton version
  and for both sqlite and postgresql databases.
  See `Controlling Tests`_ for details.


.. _variables:

Variables available in the CI/CD pipeline configurations
========================================================

The behavior of the CI/CD templates can be controlled by some variables.


Controlling Tests
-----------------

The CI/CD templates use these variables for controlling
for controlling which tests to run
resp. which files to be tested.

:CI_SKIP_POSTGRESQL_TESTS:
   If set to some value different from ``0`` or ``no``
   the tests involving the postgresql database
   will be skipped.

   Default is to also run tests involving the postgresql database.

:DOC_DIR: (default: ``doc``)
   Directory where the documentation to be build using `sphinx` resides.

:REAME_FILE: (default: ``README.rst``)
   Name of the `Readme` file.


Controlling Deployment
----------------------

The CI/CD templates use these variables for controlling
deployment of packages.

:DEPLOY_TO_GITLAB_REGISTRY:
   If set to some value different from ``0`` or ``no``
   the tagged versions of the package will be deployed to the project's
   package registry at gitlab/heptapod.

   Default is to not deploy to the project's package registry.

:DEPLOY_TO_PYPI:
   If set to some value different from ``0`` or ``no``
   the tagged versions of the package will be deployed to PyPI.
   This requires the variable ``PYPI_API_TOKEN`` to be set
   to a valid PyPI token (including the prefix ``pypi-``).

   Default is to not deploy to PyPI.

:PYPI_UPLOAD_OPTIONS:
   Passed on to ``twine upload`` the deploying to PyPI.
   The primary use of this is to allow debugging deploy issues
   by setting the CI/CD variable ``PYPI_UPLOAD_OPTIONS`` to
   ``--verbose`` when manually running a pipeline.


Extending the templates
=======================

* Example: `sphinx` documentation lives in a different directory::

    include:
      … as above

    variables:
      DOC_DIR: docs

* Example: Disabling the `build` job::

    include:
      … as above

    build:
      rules:
      - when: never
